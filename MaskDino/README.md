# Intento de implementación de MaskDino

En la presente carpeta se encuentra el intento de implementación del repositorio MaskDino.
Como es posible ver, este contiene los submodulos MaskDino y detectron2, los cuales funcionan en conjunto.
Se intentó el entrenamiento de un modelo con esta arquitectura pero luego de varios intentos, no hubo un resultado positivo.
En particular, en un principio nos encontramos con problemas al inscribir el dataset dentro de los archivos locales de detectron2, luego, al resolver el bug (había que correr el código de inscripción en el mismo archivo de entrenamiento), el código por alguna razón cambiaba la ubicación de cada imagen del dataset, con lo cual no encontraba las imágenes para realizar el entrenamiento, especificamente, insertaba la letra p al inicio del path.
Esto nos llevo a realizar un debugging demasiado duradero, y por motivos de disponibilidad técnica (trabajo sobre servidor Wildsense), nos vimos obligados a dejar atrás el objetivo.
Importante mencionar que en el archivo .yaml, se encuentra la configuracion del modelo, en donde se definio el nombre del dataset de entrenamiento y de validación. Además, el archivo "train_net.py", registra el dataset correspondiente y ejecuta el entrenamiento. Ambos de estos archivos deben ser movidos a su respectiva carpeta dentro de los archivos de MaskDino.
