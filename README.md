# Segmentación de salmones - Grupo 10

## Introducción

Este repositorio se encarga de la implementacion de modelos de Aprendizaje Profundo (Deep Learning) para la segmentacion de salmones en imágenes.
La carpeta "/spinenet_code" contiene lo relacionado con la imeplementación del backbone SpineNet.
Por otro lado, el intento de la implementacion de MaskDino, se encuentra en su carpeta también.
Cabe mencionar que cada carpeta tiene su propio ReadMe, con instrucciones de instalación.
