

__versión__ = "1.0"

"""
El módulo *guardarImagen* permite seleccionar una imagen, mostrarla en un QLabel y
guardarla en una Base de Datos (SQLite) junto a un nombre de usuario.
"""
import cv2
from os import getcwd
from sqlite3 import connect

from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal, QByteArray, QIODevice, QBuffer
from PyQt5.QtWidgets import (QApplication, QDialog, QLabel, QPushButton, QFileDialog,
                             QLabel, QLineEdit)


# ===================== CLASE QLabelClickable ======================

class QLabelClickable(QLabel):
    clicked = pyqtSignal()
    
    def __init__(self, parent=None):
        super(QLabelClickable, self).__init__(parent)

    def mousePressEvent(self, event):
        self.clicked.emit()


# ===================== CLASE labelClickable =======================

class guardarImagen(QDialog):
    def __init__(self, parent=None):
        super(guardarImagen, self).__init__(parent)
        
        self.setWindowTitle("Segmentacion de Salmones")
        self.setWindowIcon(QIcon("icono.png"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(1000, 700)

        self.initUI()

    def initUI(self):
      #=====================  Tablas metricas=====================


      # ==================== WIDGET QLABEL =======================
        labelNombre1 = QLabel("Imagen A Segmentar", self)
        labelNombre1.move(140, 15)
        self.labelImagen = QLabelClickable(self)
        self.labelImagen.setGeometry(105, 35, 321, 321)
        self.labelImagen.setToolTip("Imagen")
        self.labelImagen.setCursor(Qt.PointingHandCursor)

        self.labelImagen.setStyleSheet("QLabel {background-color: white; border: 1px solid "
                                       "#01DFD7; border-radius: 2px;}")
        
        self.labelImagen.setAlignment(Qt.AlignCenter)

      #================= Creacion cuadro imagen segmentada=========================
        labelNombre2 = QLabel("Imagen Segmentada", self)
        labelNombre2.move(535, 15)
        self.labelImagen2 = QLabelClickable(self)
        self.labelImagen2.setGeometry(500, 35, 321, 321)
        self.labelImagen2.setToolTip("Imagen2")
        self.labelImagen2.setCursor(Qt.PointingHandCursor)

        self.labelImagen2.setStyleSheet("QLabel {background-color: white; border: 1px solid "
                                       "#01DFD7; border-radius: 2px;}")
        
        self.labelImagen2.setAlignment(Qt.AlignCenter)

      # ==================== WIDGETS QLABEL ======================

        '''labelNombre = QLabel("Nombre de usuario", self)
        labelNombre.move(193, 15)'''

      # ================== WIDGETS QLINEEDIT =====================

        '''self.lineEditNombre = QLineEdit(self)
        self.lineEditNombre.setGeometry(193, 30, 192, 25)'''

      # ================= WIDGETS QPUSHBUTTON ====================

        buttonSeleccionar = QPushButton("Seleccionar", self)
        buttonSeleccionar.setToolTip("Seleccionar imagen")
        buttonSeleccionar.setCursor(Qt.PointingHandCursor)
        buttonSeleccionar.setGeometry(170, 360, 168, 25)

        '''buttonBuscar = QPushButton("Buscar", self)
        buttonBuscar.setToolTip("Buscar usuario")
        buttonBuscar.setCursor(Qt.PointingHandCursor)
        buttonBuscar.setGeometry(193, 60, 93, 25)'''

        buttonGuardar = QPushButton("Guardar", self)
        buttonGuardar.setToolTip("Guardar usuario")
        buttonGuardar.setCursor(Qt.PointingHandCursor)
        buttonGuardar.setGeometry(170, 390, 168, 25)

      # ===================== EVENTO QLABEL ======================

      # Llamar función al hacer clic sobre el label
        self.labelImagen.clicked.connect(self.seleccionarImagen)

      # ================== EVENTOS QPUSHBUTTON ===================

        buttonSeleccionar.clicked.connect(self.seleccionarImagen)
        #buttonBuscar.clicked.connect(self.Buscar)
        buttonGuardar.clicked.connect(self.Guardar)
     

  # ======================= FUNCIONES ============================

    def seleccionarImagen(self):
        imagen, extension = QFileDialog.getOpenFileName(self, "Seleccionar imagen", getcwd(),
                                                        "Archivos de imagen (*.png *.jpg)",
                                                        options=QFileDialog.Options())
        
        if imagen:
            # Adaptar imagen
            cvimagen=cv2.imread(imagen)
            
            h,w,c=cvimagen.shape
            print(h)
            pixmapImagen = QPixmap(imagen).scaled(300, 321, Qt.KeepAspectRatio,
                                                  Qt.SmoothTransformation)

            # Mostrar imagen
            self.labelImagen.setPixmap(pixmapImagen)

        #imagen resultante (segmentada)
        imagenS = cv2.imread('salmon segmentado.jpg')
        if imagenS.any():
          pixmapImagens=QPixmap('salmon segmentado.jpg').scaled(321, 321, Qt.KeepAspectRatio,
                                                  Qt.SmoothTransformation)
          self.labelImagen2.setPixmap(pixmapImagens)

    def Buscar(self):
        pass

    def Guardar(self):
        # Obtener el nombre de usuario y la foto
        nombre = "Imagen Sin Segmentar"
        foto = self.labelImagen.pixmap() 
        foto.save("{}.png".format(nombre))
        if foto:
            # Convertir la foto al tipo de dato adecuado
            bArray = QByteArray()
            bufer = QBuffer(bArray)
            bufer.open(QIODevice.WriteOnly)
            bufer.close()
            foto.save(bufer, "PNG")
        else:
            bArray = ""

        if nombre and bArray:
            # Establecer conexión con la base de datos
            conexion = connect("DB_USUARIOS.db")
            cursor = conexion.cursor()
            
            # Crear tabla, si no existe
            cursor.execute("CREATE TABLE IF NOT EXISTS Usuarios (NOMBRE TEXT, FOTO BLOB)")
            conexion.commit()
            
            # Verificar que el usuario no exista
            if cursor.execute("SELECT * FROM Usuarios WHERE NOMBRE = ?", (nombre,)).fetchone():
                print("El usaurio {} ya existe.".format(nombre))
            else:
                #Guardar en la base de datos, el nombre de usuario y la foto
                cursor.execute("INSERT INTO Usuarios VALUES (?,?)", (nombre, bArray))
                conexion.commit()

                self.labelImagen.clear()
                self.lineEditNombre.clear()

                print("Usuario guardado con éxito.")

            # Cerrar la conexión con la base de datos
            conexion.close()

            self.lineEditNombre.setFocus()
        else:
            self.lineEditNombre.setFocus()
            

# ================================================================

if __name__ == '__main__':
    
    import sys
    
    aplicacion = QApplication(sys.argv)
    
    ventana = guardarImagen()
    ventana.show()
    sys.exit(aplicacion.exec_())