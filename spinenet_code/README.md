# Segmentación de salmones - Modelo MASK-RCNN y Backbone Spinenet

El entrenamiento se realizó por medio de mmdetection, una librería abierta, la cual cuenta con modelos cde detección de objetos de última generación. Además, se utilizó el dataset "salmons" de formato COCO.
Importante mencionar que se trata de una estructura compuesta por el modelo Mask-RCNN y un backbone SpineNet. También, para el buen rendimineto de los pesos, se entreno con 350 epochs.

## Implementación

Se cuenta con la ayuda de una interfaz desarrollada en QT Creator para la inferencia del modelo sobre imágenes personalizadas.

Para lograr entrenar e inferir sobre la arquitectura, es necesario realizar lo siguente:

- Descargar dataset de entrenamiento en formato COCO.
- Configurar archivo de configuracion del modelo a entrenar:
    - Editar parametros "data" (linea 162), el cual entrega la ubicacion del dataset, ademas, work_dir (linea 260) y load_from (linea 261). Éste último entrega la ubicacion del archivo de los pesos pre-entrenados para realizar transfer learning al entrenar.
- Se debe añadir el archivo de configuracion de la estructura de la red en la carpeta correspondiente en mmdetection. Es decir:
    - Copiar "segmentacion-de-salmones/spinenet_code/mmdetection/configs/mask_rcnn/mask_rcnn_spinenet_49_B_8gpu_640.py" y pegar en "segmentacion-de-salmones/mmdetection/configs/mask_rcnn".
- Luego, añadir configuracion del backbone Spinenet en los archivos de mmdetection.
    - Copiar "segmentacion-de-salmones/spinenet_code/mmdetection/mmdet/models/backbones/spinenet.py" y pegar en "segmentacion-de-salmones/mmdetection/mmdet/models/backbones".
- Si se desea entrenar, se debe correr el siguiente comando:
    - python3 mmdetection/tools/train.py "segmentacion-de-salmones/mmdetection/configs/mask_rcnn/mask_rcnn_spinenet_49_B_8gpu_640.py"
- Esto permite el inicio del entrenamiento con 350 epochs, lo cual es configurable desde el mismo archivo de configuración. No esta de mas decir que lo escrito entre comillas es la ubicacion del arcivo de configuración.
- Luego de estos pasos, ya se deberia poder entrenar el modelo.

- Para realizar la inferencia, se debe correr el archivo "test_inference.py", el cual inicializa el modelo y lo ejecuta sobre una imagen, estos métodos son nativos de mmdetection.

## Inferencia con interfaz gráfica

Para lograr correr la interfaz gráfica, es necesario reaizar lo siguiente:

- Editar ubicaciones de checkpoint y archivo de configuracion, dentro de "interface.py" (lineas 15 y 17)
- Crear un ambiente de anaconda, por medio del siguiente comando: conda create --name <env> --file "requirements.txt"
- Activar ambiente: conda activate <env>
- Correr archivo python "interface.py"

De momento que este abierta la aplicación, el siguiente paso es seleccionar la imagen a segmentar.
Se debe presionar el botón "Seleccionar", el cual abre un menu para busqueda del archivo (imagen).
Una vez seleccionada, se esperan 5 segundos, y se visualiza la imagen segmentada, por medio del modelo declarado anteriormente.

### No esta de más mencionar...

- El archivo "test_inference.py" realiza una inferencia sobre la imagen declarada dentro del archivo, es basicamente como funciona el backend de "interface.py"
- Existe un archivo .json llamado eval..., este contiene las métricas exportadas por mmdetection del modelo previamente entrenado.
- "epoch_350.pth" es el modelo entrenado, es decir, los pesos de la red neuronal en la última iteración del entrenamiento (epoch 350).
- La carpeta "Luis_Felipe_Inference" fue la implementación de la clase Deep() de L.F., en donde se pudo realizar la inferencia de uno de los modelo entrenados por él, esto nos entrego conocimiento de como funcionaba su código, además de la interacción con mmdetection.
