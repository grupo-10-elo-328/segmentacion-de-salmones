#Implementacion de codigo Luis Felipe, se realiza inferencia con exito (se debe usar con Docker)

import os
from PIL import Image
from Deep import Deep

img = "/home/mass_estimation/experiments/experiment_1/data/7.jpg"

deep_class = Deep(
    experimentOrderFile="/home/mass_estimation/experiments/experiment_1/ExperimentOrderFiles/create_dataset_exp1.csv",
    ID_to_run=2,
    dataset_dir="/home/mass_estimation/experiments/salmon_segmentation"
)

#Inferencia
(results, segmentation, bboxes, labels) = deep_class.test(imgs=img, inference_method="mmdet_inference")
result_img = deep_class.draw_results(img=img, result=results)
data = Image.fromarray(result_img)
data.save("/home/mass_estimation/experiments/experiment_1/inference_testing_results/test2.png")

print("Results: ", results)
print("segm: ", segmentation)
print("bboxes: ", bboxes)
print("Labels: ", labels)

#Entrenamiento
# deep_class.train()