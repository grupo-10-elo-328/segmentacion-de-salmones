
__versión__ = "1.0"

import cv2
from os import getcwd
from PyQt5.QtGui import QIcon, QPixmap
from PyQt5.QtCore import Qt, pyqtSignal, QByteArray, QIODevice, QBuffer
from PyQt5.QtWidgets import (QApplication, QDialog, QLabel, QPushButton, QFileDialog, QLabel, QLineEdit)

from mmdet.apis import init_detector, inference_detector
import mmcv
import time

# Archivo de configuracion
config_file = '/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/mask_rcnn_spinenet_49_B_8gpu_640.py'
#Modelo entrenado (pesos de la ultima iteracion del entrenamiento)
checkpoint_file = '/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/epoch_350.pth'

#Se inicializa el modelo
model = init_detector(config_file, checkpoint_file, device='cuda:0')

class QLabelClickable(QLabel):
    clicked = pyqtSignal()
    
    def __init__(self, parent=None):
        super(QLabelClickable, self).__init__(parent)

    def mousePressEvent(self, event):
        self.clicked.emit()

class InterfazSegmentacionDeSalmones(QDialog):
    def __init__(self, parent=None):
        super(InterfazSegmentacionDeSalmones, self).__init__(parent)
        self.setStyleSheet("background-color: #637674;")
        self.setWindowTitle("Segmentacion de Salmones")
        self.setWindowIcon(QIcon("icono.png"))
        self.setWindowFlags(Qt.WindowCloseButtonHint | Qt.MSWindowsFixedSizeDialogHint)
        self.setFixedSize(1000, 700)

        self.initUI()

    def initUI(self):
      #Diseo de los labels de la interfaz

        label = QLabel("Segmentación de Salmones                               ", self)
        label.setStyleSheet("QLabel { background-color: #1D322D; font-size:50px; color: #fff;}")
        labelNombre1 = QLabel("Imagen A Segmentar", self )
        labelNombre1.setStyleSheet("QLabel { font-size:30px; color: #fff;}")
        labelNombre1.move(130, 150)
        self.labelImagen = QLabelClickable(self)
        self.labelImagen.setGeometry(105, 200, 321, 321)
        self.labelImagen.setToolTip("Imagen")
        self.labelImagen.setCursor(Qt.PointingHandCursor)

        self.labelImagen.setStyleSheet("QLabel {background-color: #ABB0AF; border: 1px solid "
                                       "#ABB0AF; border-radius: 2px;}")
        
        self.labelImagen.setAlignment(Qt.AlignCenter)

        labelNombre2 = QLabel("Imagen Segmentada", self)
        labelNombre2.setStyleSheet("QLabel { font-family: 'Roboto'; font-size:30px; color: #fff;}")
        labelNombre2.move(525, 150)
        self.labelImagen2 = QLabelClickable(self)
        self.labelImagen2.setGeometry(500, 200, 321, 321)
        self.labelImagen2.setToolTip("Imagen2")
        self.labelImagen2.setCursor(Qt.PointingHandCursor)

        self.labelImagen2.setStyleSheet("QLabel {background-color: #ABB0AF; border: 1px solid "
                                       "#ABB0AF; border-radius: 2px;}")
        
        self.labelImagen2.setAlignment(Qt.AlignCenter)

        #Diseño de los botones
        buttonSeleccionar = QPushButton("Seleccionar", self)
        buttonSeleccionar.setToolTip("Seleccionar imagen")
        buttonSeleccionar.setCursor(Qt.PointingHandCursor)
        buttonSeleccionar.setGeometry(170, 540, 168, 25)
        buttonSeleccionar.setStyleSheet("background-color: #125B4C")

        buttonGuardar = QPushButton("Guardar", self)
        buttonGuardar.setToolTip("Guardar usuario")
        buttonGuardar.setCursor(Qt.PointingHandCursor)
        buttonGuardar.setGeometry(170, 570, 168, 25)
        buttonGuardar.setStyleSheet("background-color: #125B4C")

        self.labelImagen.clicked.connect(self.seleccionarImagen)

        buttonSeleccionar.clicked.connect(self.seleccionarImagen)
        buttonGuardar.clicked.connect(self.Guardar)
     

    def seleccionarImagen(self):
        imagen, extension = QFileDialog.getOpenFileName(self, "Seleccionar imagen", getcwd(),
                                                        "Archivos de imagen (*.png *.jpg *.jpeg)",
                                                        options=QFileDialog.Options())
        
        if imagen:
            # Se realiza la inferencia de la imagen seleccionada
            result = inference_detector(model, imagen)

            cvimagen=cv2.imread(imagen)
            # Adaptar imagen
            h,w,c = cvimagen.shape
            print(h)
            pixmapImagen = QPixmap(imagen).scaled(300, 321, Qt.KeepAspectRatio, Qt.SmoothTransformation)

            # Mostrar imagen
            self.labelImagen.setPixmap(pixmapImagen)

        # Se guarda la imagen segmentada
        model.show_result(imagen, result, out_file='result.jpg')
        time.sleep(5)
        #Imagen resultante (segmentada)
        imagenS = cv2.imread('/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/result.jpg')
        if imagenS.any():
            pixmapImagens=QPixmap('/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/result.jpg').scaled(321, 321, Qt.KeepAspectRatio, Qt.SmoothTransformation)
            self.labelImagen2.setPixmap(pixmapImagens)

    def Guardar(self):
        # Obtener el nombre de usuario y la foto
        nombre = "Imagen Sin Segmentar"
        foto = self.labelImagen.pixmap() 
        foto.save("{}.png".format(nombre))
        if foto:
            # Convertir la foto al tipo de dato adecuado
            bArray = QByteArray()
            bufer = QBuffer(bArray)
            bufer.open(QIODevice.WriteOnly)
            bufer.close()
            foto.save(bufer, "PNG")
        else:
            bArray = ""


if __name__ == '__main__':
    
    import sys
    
    aplicacion = QApplication(sys.argv)
    
    ventana = InterfazSegmentacionDeSalmones()
    ventana.show()
    sys.exit(aplicacion.exec_())
