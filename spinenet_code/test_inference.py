from mmdet.apis import init_detector, inference_detector
import mmcv

config_file = '/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/mask_rcnn_spinenet_49_B_8gpu_640.py'
checkpoint_file = '/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/epoch_350.pth'

# Inicializar modelo desde archivo de configuracion y modelo
model = init_detector(config_file, checkpoint_file, device='cuda:0')

# Inferencia de una imagen particular
img = '/mnt/HDD/nicolas-capetillo/segmentacion-de-salmones/spinenet_code/data/7.jpg'
result = inference_detector(model, img)

model.show_result(img, result, out_file='result.jpg')